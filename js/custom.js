

$(document).ready(function () {
    $('ul.mainrightnav li:has(ul)').click(function (e) {
        e.preventDefault();

        if ($(this).hasClass('activado')) {
            $(this).removeClass('activado');
            $(this).children('ul').slideUp();
        } else {
            $('.mainrightnav li ul').slideUp();
            $('.mainrightnav li').removeClass('activado');
            $(this).addClass('activado');
            $(this).children('ul').slideDown();
        }

        $('ul.mainrightnav li ul li a').click(function () {
            window.location.href = $(this).attr('href');
        })
    });
});
